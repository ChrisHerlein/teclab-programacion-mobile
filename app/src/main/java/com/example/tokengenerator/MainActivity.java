package com.example.tokengenerator;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.tokengenerator.databinding.ActivityMain2Binding;
import android.widget.Button;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {
    Button ntbutton;
    Button nubutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ntbutton = (Button) findViewById(R.id.buttonGoNewToken);
        ntbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewToken();
            }
        });

        nubutton = (Button) findViewById(R.id.buttonGoNewUser);
        nubutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewUser();
            }
        });

    }

    public void openNewToken(){
        Intent intent = new Intent(this, MainTokenGenerator.class);
        startActivity(intent);
    }
    public void openNewUser(){
        Intent intent = new Intent(this, AddUserToken.class);
        startActivity(intent);
    }
}